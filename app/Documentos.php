<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Documentos extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'documentos';

    protected $fillable=['titulo','autor','descripcion','file'];
}
