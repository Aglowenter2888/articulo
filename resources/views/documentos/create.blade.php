@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')
  <!-- Bootstrap CSS -->
  <div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Crear nuevo documento</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">	
  <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Crear un nuevo Documento</div>
                    </div>
                <div class="card-body">
                    <form>
                        <div class="form-row">
      <form action="/files" method="POST" enctype="multipart/form-data">

        @csrf 
        <div class="form-group col-md-6 ">
        <label for="inputEmail4">Nombre Del Titulo:</label>
        <input type="text" name="titulo" placeholder="titulo">
        </div>

        <div class="form-group col-md-6 ">
        <label for="inputEmail4">Nombre Del Autor:</label>
        <input type="text" name="autor" placeholder="autor">
        </div>

        <div class="form-group col-md-6 ">
        <label for="inputEmail4">Agregar Descripcion:</label>
        <input type="text" name="descripcion" placeholder="descripcion">
      

          <input type="file" class="btn btn-light" name="file">
          <br>
          <input type="submit" class="btn btn-primary" value="Enviar">  <br>
      </form>            
                                                              
                  
             

@endsection
