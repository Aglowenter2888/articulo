@extends('layouts.app')

@section('Titulo', 'Repositorio de investigaciones ')

@section('content')

<!--  Este del formulario inicio de  gestion academida -->

<div class="panel-header colorut" >
		<div class="page-inner py-5">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
			    <div>
			        <h2 class="text-black pb-2 fw-bold">Captura de Expedientes</h2>
		        </div>		
	        </div>
		</div>
    </div>
	<div class="page-inner mt--5">					
	    <!-- Contenido de Gestion Academica-->
	    <!-- Cuadros de menu -->  
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Captura de Gestion academicas</div>
                    </div>
                <div class="card-body">
                    <form action="{{ url('pdf_download_GestionA') }}" method="post" accept-charset="utf-8">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputEmail4">Nombre de la gestion</label>
                                <input type="text" class="form-control border border-secondary" id="Titulo" name="Titulo" placeholder="Titulo">
                                <span class="text-danger">{{ $errors->first('Titulo') }}</span>
                            </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">Fecha de inicio </label>
                                <input type="date" class="form-control border border-secondary" id="Date" name="Date" >
                                <span class="text-danger">{{ $errors->first('Date') }}</span>
                            </div>
                            <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity">Fecha de termino</label>
                                <input type="date" class="form-control border border-secondary" id="DateF" name="DateF" >
                                <span class="text-danger">{{ $errors->first('DateF') }}</span>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Area de Entrega</label>
                                <select id="inputState" class="form-control border border-secondary" name="Area" id="Area">
                                    <option selected>Divsion de ingenieria</option>
                                    <option>Turismo</option>
                                </select>
                                <span class="text-danger">{{ $errors->first('Area') }}</span>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputState">Captura de evidencia</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="validatedCustomFile" >
                                    <label class="custom-file-label  border border-secondary" for="validatedCustomFile">Archivo..</label>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Generar Archivo</button>
                        </form>                                       
                    </div>
                </div>
            </div>
        </div>
    </div>



<!-- Fin de formulario de gestion academida -->





@endsection
